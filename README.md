# Proof of Concept Thesis
This proof of concept application serves as proof for the research portion of my graduation thesis.
Furthermore, the subject of the thesis has considerable merit for the company at which I am doing my graduation internship.
With that in mind, the Proof of Concept was formed to fit the real-world application of the company so that it could, if so desired, be implemented in their project.
 

## Migrating Spring Cloud applications to a reactive stack
This project contains the following Spring Boot applications:
* poc
* poc-server
* poc-router
* poc-reactive

 Each application in the project serves a specific purpose which will be covered below.
 
 ### Use case
 The use case to demonstrate the Proof of Concept is as follows: a literature project app where a user can add, edit and remove
 * projects
 * project members
 * project literature
 * authors in the database
 * literature in the database
 
 ### poc-server
This application serves basic HTTP to the gateway application, in which a set of endpoints of each entity represents a vendor in the real-world application. For the sake of not overcomplicating the proof of concept, these endpoints all originate from this single service, the poc-server.
 
 ### poc
 This application attempts to emulate the operations of the application on which the proof of concept could be implemented. It serves as a gateway of sorts, where a client can make requests to the application which will then communicate to the HTTP service (poc-server) and return a response to the client.
 
 One important thing to keep in mind was the assumption that the calls we could make to the HTTP service were only the most basic. This meant having to orchestrate a substantial amount of requests in the gateway application, which reflects what happens in the real-world application.
 
 Lastly, the use of Netflix OSS's Zuul for routing network traffic is required, as some calls made by the client would require orchestration in the gateway, and others would not. The calls not requiring orchestration could then be sent directly to the HTTP service.
 
 ### poc-reactive
 This application contains almost all functionality present in the traditional Spring Boot Web application (poc) with the main difference being that it is built using the non-blocking reactive stack Spring Boot Webflux. The only thing missing in the reactive implementation is the routing functionality Zuul offered in the poc application.
 
 ### poc-router
 Using Zuul in the poc application meant having to migrate to a different tool for routing network traffic, as Zuul is blocking by nature. While Netflix recently released Zuul 2, which is a non-blocking variant, making as much use of the Spring ecosystem seemed the better option, as it would ensure more long-term support. Luckily, Pivotal offers a solid alternative, namely Spring Cloud Gateway. Due to the fundamental differences between the two tools, however, it meant having to split the router from the rest of the application. 
